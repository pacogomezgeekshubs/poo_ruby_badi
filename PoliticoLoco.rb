class PoliticoLoco
    def initialize(nombre)
        @nombre=nombre
        @frases=[]
    end
    def otraFraseLoca(frase)
        @frases.push(frase)
    end
    def listarFrases
        @frases.each do |frase|
            puts frase
        end
    end
    def fraseRandom
        @frases[rand(0..@frases.size-1)]
    end
    def getNombre
        @nombre.upcase
    end
end

#Comienza el juego
abascal=PoliticoLoco.new("Abascal")
iglesias=PoliticoLoco.new("Iglesias")
#Añadimos frases a Abascal
abascal.otraFraseLoca("Estoy muy loco")
abascal.otraFraseLoca("Me gustan los niños")
abascal.otraFraseLoca("Me gusta morder platos de plastico")
#abascal.listarFrases
#Añadimos frases a Iglesias
iglesias.otraFraseLoca("Me gustan los helados de pera")
iglesias.otraFraseLoca("Tengo un chalet en la Moraleja")
iglesias.otraFraseLoca("Me gusta chupar madera")
#iglesias.listarFrases
#Abascal contra Iglesias
loop do
    puts "#{abascal.getNombre} DICE: "+abascal.fraseRandom
    puts "#{iglesias.getNombre} DICE: "+iglesias.fraseRandom
    puts "-------"
    puts "Otra frase mas?[S,N]"
    salir=gets.chomp
    if(salir=="N") 
        break
    else
        puts "Masoca"
        puts "-------"
    end
end