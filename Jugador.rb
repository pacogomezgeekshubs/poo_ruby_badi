#Clase jugador
class Jugador
    LIGA="NBA"

    def initialize
        puts "-----------Creo una nueva clase de tipo Jugador----------"
        @name="Paco"
        puts LIGA
        #LIGA="Endesa" Daria error
    end
    def saludo(nombre="Paco")
        @name=nombre
        puts "Soy el jugador #{@name}"
        return quienSoy #se utiliza return
    end
    private
    def quienSoy
        "Paco" #devuelve un String sin utilizar return
    end
end

#Instancia
j=Jugador.new
#Uso de un metodo
j.saludo("Arnald")
#Uso de un metodo
nombre=j.saludo
puts nombre
#Uso de atributos
j
#Uso de metodos
#j.quienSoy Da error