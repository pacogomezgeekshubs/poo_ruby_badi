class Juego
    attr_reader :numOportunidades, :encontrado

    def initialize
        @numOportunidades=0
        @encontrado=false
        @numAleatorio=Juego::dado(10)
    end
    def self.dado(caras)
        if(caras>0)
            rand(1..caras)
        else
            rand(1..6)
        end
    end

    def adivina(numero)
        if numero!=@numAleatorio
            @numOportunidades+=1
        else
            @encontrado=true
        end
        return @encontrado
    end
    
end