#variables
num_oport = 0
numeroAdivinar = rand(1..10)
encontrado = false


until encontrado
  print "Adivina el número de 1 a 10 que estoy pensando: "
  numUsuario = gets.chomp.to_i

  if numUsuario == numeroAdivinar
    puts "!PERFECTO! Lo conseguiste en #{num_oport} intentos!"
    encontrado = true
  else
    puts "Lo siento! No es el número, intentalo de nuevo."
    num_oport += 1
  end
end