require "./Juego"

puts Juego::dado(10)

juego=Juego.new

until juego.encontrado
    print "Adivina el número de 1 a 10 que estoy pensando: "
    numUsuario = gets.chomp.to_i
  
    juego.adivina(numUsuario)
    if  juego.encontrado
      puts "!PERFECTO! Lo conseguiste en #{juego.numOportunidades} intentos!"
    else
      puts "Lo siento! No es el número, intentalo de nuevo."
    end
end