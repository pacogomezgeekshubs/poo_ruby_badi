class Programador
    attr_accessor :nombre,:apellidos
    attr_reader:tecnologia

    def initialize(nombre,apellidos)
        @nombre=nombre
        @apellidos=apellidos
        @tecnologia="R/R"
    end

end

#Uso la clase programador
programador=Programador.new("Ejemplo","2")

programador.nombre="Paco"
programador.apellidos="Gomez Arnal"
puts programador.nombre
puts programador.apellidos

#Esto va a dadr error
#programador.tecnologia="PHP"
puts programador.tecnologia

#Todo hereda de Object
puts programador.class
puts programador.nil?
puts programador.is_a?Object
